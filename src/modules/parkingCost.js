/**
 * @description Cost for parking slot
 */
class ParkingCost {
  constructor (cost = 10, currency = '$', constCostByHour = 2) {
    this.cost = cost;
    this.currency = currency;
    this.constCostByHour = constCostByHour;
  }

  /**
   * @param {number} hours
   * @description returns difference hours between start and end time
   */
  getCostByHours (hours) {
    let result = this.cost;

    if (hours > this.constCostByHour) {
      result += this.cost * (hours - this.constCostByHour);
    }

    return result + this.currency;
  }
}

module.exports = ParkingCost;
