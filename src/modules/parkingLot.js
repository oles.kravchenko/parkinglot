var Car = require('./car.js');
var Time = require('./time.js');
var ParkingCost = require('./parkingCost.js');

/**
 * @description a base class for Parking lot
 */
class ParkingLot {

	constructor () {
        this.MAX_PARKING_SLOTS = 0; // maximum parking slots allowed
        this.parkingSlots = new Array(); // array for parking slots
				this.parkingCost = new ParkingCost()
    }

	/**
	 *
	 * @param {String} input user's input via terminal
	 * @description creates a parking lot with given maximum slot numbers.
	 * It throws an error if zero or negative input is provided
	 */
	createParkingLot (input) {
		this.MAX_PARKING_SLOTS = parseInt(input.split(' ')[1]);
		if (this.MAX_PARKING_SLOTS <= 0) {
			// minimum: 1 slot
			throw new Error('Minimum one slot is required to create parking slot');
		}
        for (var i = 0; i < this.MAX_PARKING_SLOTS; i++) {
            this.parkingSlots.push(null);
        }
        return this.MAX_PARKING_SLOTS;
	}

	/**
	 *
	 * @param {String} input user's input via terminal
	 * @description allocates nearest slot number to incoming cars.
	 * It throws an error if parking lot is empty or full.
	 * It also throws an error if only one field (registration number) is provided.
	 */
    parkCar (input) {
        var len = this.parkingSlots.length;
    	if (this.MAX_PARKING_SLOTS > 0) {
			var car, carNumber, time;
	    	if (this.findNearestAvailableSlot(this.parkingSlots) == true) {
		  		for (var i = 0; i < len; i++) {
		  			if (this.parkingSlots[i] == null) {
						carNumber = input.split(' ')[1];
						if (carNumber) {
							car = new Car(carNumber);
							time = new Time();
							this.parkingSlots[i] = {
								car: car,
								time: time
							};

							i = i + 1;
							return i;
						}
						else {
							throw new Error('Please provide registration number');
						}
		  			}
		  		}
			  }
			else {
		  		throw new Error('Sorry, parking lot is full');
		  	}
          }
          else {
	  		throw new Error('Minimum one slot is required to create parking slot');
	  	}
	}

	/**
	 *
	 * @param {String} input user's input via terminal
	 * @description it makes the slot free for the car of given registration number.
	 * It throws an error if car is not found.
	 */
	leaveCar (input) {
		if (this.MAX_PARKING_SLOTS > 0) {

			var arrInput = input.split(' ');
			var endTime = Number(arrInput[2]);

			if (!endTime) throw new Error('Sorry, car doesnt have leave time');

			var carNumber = arrInput[1];

			for (var index = 0; index < this.MAX_PARKING_SLOTS; index++) {

				if (this.parkingSlots[index].car.NUMBER === carNumber) {

					this.parkingSlots[index].time.setHoursEndTimeByStartTime(endTime);
					var hours = this.parkingSlots[index].time.getDiffHours();
					var cost = this.parkingCost.getCostByHours(hours);
					this.parkingSlots[index] = null;

					return {
						cost,
						slotNumber: index + 1
					}
				}
			}
		}
		else {
			throw new Error('Sorry, car with given registration is not found');
		}
	}

	/**
	 * @description Returns an array containing parking details i.e. slot no, registration number
	 */
    getParkingStatus () {
    	var arr = new Array();
    	if (this.MAX_PARKING_SLOTS > 0) {
			arr.push('Slot No. Registration No. ');

			// use binary search here
        	for (var i = 0; i < this.parkingSlots.length; i++) {
        		if (this.parkingSlots[i] != null) {
        			var e = i + 1;
        			arr.push(e + '.  ' + this.parkingSlots[i].car.NUMBER);
        		}
        	}
        	return arr;
		}
		else {
			throw new Error('Sorry, parking lot is empty');
		}
	}


	/**
	 *
	 * @param {String} input user's input via terminal
	 * @description returns slot number for given car number.
	 * It returns null if car is not found.
	 */
    getSlotByCarNumber (input) {
		// TODO:  What parking lot is empty
		if (this.MAX_PARKING_SLOTS > 0) {
	    	var ele = 'Not found';
	        for (var i = 0; i < this.parkingSlots.length; i++) {
	        	if (this.parkingSlots[i] && this.parkingSlots[i].car.NUMBER == input.split(' ')[1]) {
	        		ele = i + 1;
	        	}
	        }
        	return ele;
        }
        else {
			return null;
		}
	}

	/**
	 * @description returns a comma separated string of free parking slots.
	 * It returns `null` if parking lot is not created
	 */
	findAllAvailableSlots () {
		if (this.MAX_PARKING_SLOTS > 0) {
	    	var availableSlots = new Array();
	        for (var i = 0; i < this.parkingSlots.length; i++) {
	        	if (!(this.parkingSlots[i] && this.parkingSlots[i].car.NUMBER)) {
	        		availableSlots.push(i + 1);
	        	}
	        }
        	return availableSlots.join(', ');
        }
        else {
			return null;
		}
	}

	/**
	 * @description returns a comma separated string of allocated parking slots.
	 * It returns `null` if parking lot is not created.
	 */
	findAllAllocatedSlots () {
		if (this.MAX_PARKING_SLOTS > 0) {
	    	var allocatedSlots = new Array();
	        for (var i = 0; i < this.parkingSlots.length; i++) {
	        	if (this.parkingSlots[i] && this.parkingSlots[i].car.NUMBER) {
	        		allocatedSlots.push(i + 1);
	        	}
	        }
        	return allocatedSlots.join(', ');
        }
        else {
			return null;
		}
	}

	/**
	 * @description returns the nearest available slot
	 * used by parkCar() method to find nearest slot
	 */
	findNearestAvailableSlot () {
		var ele = false;
		for (var i = 0; i < this.parkingSlots.length; i++) {
			if (this.parkingSlots[i] == null) {
				ele = true;
			}
		}
		return ele;
	}
}

module.exports = ParkingLot;
