/**
 * @description Register Time for car
 */
class Time {
  constructor () {
    this.startTime = new Date();
    this.endTime = null;
  }

  /**
   * @param {number} hours
   * @description End Time add Hours by Start Time
   */
  setHoursEndTimeByStartTime (hours) {
    this.endTime = new Date(this.startTime);
    return this.endTime.setHours(this.startTime.getHours() + hours)
  }

  /**
   * @description returns difference hours between start and end time
   */
  getDiffHours () {
    if (!this.endTime)  throw new Error('endTime is undefined');

    return Math.abs(this.startTime - this.endTime) / 36e5
  }
}

module.exports = Time;
